
public class Main {

    public static void main(String[] args) {

        Quizz quizz = new Quizz("Le quizz du quizz nul");

        //Premiere question
        Question question = new Question("Qui est le plus riche");
        question.addResponse(new Response("Jeff bezors"));
        question.addResponse(new Response(true, "Elon Musk"));
        question.addResponse(new Response("Un éléphant"));
        quizz.addQuestion(question);

        //On recréer une instance de question
        question = new Question("Qui est le plus pauvre");
        question.addResponse(new Response(true, "Moi"));
        question.addResponse(new Response("Elon Musk"));
        question.addResponse(new Response("Un lion"));
        quizz.addQuestion(question);


        //On lance le quizz
        quizz.start();

    }

}
