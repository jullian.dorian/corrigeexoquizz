import java.util.ArrayList;
import java.util.List;

public class Question {

    public final String title;
    public final List<Response> responses = new ArrayList<>();

    public Question(String title) {
        this.title = title;
    }

    public void addResponse(Response response) {
        this.responses.add(response);
    }

    public boolean valid(int indexResponse) {

        //On oublie pas qu'on affiche l'index + 1
        indexResponse -= 1;

        //On vérifie que notre index est bien compris entre 0 et notre liste
        if(indexResponse < responses.size() && indexResponse >= 0) {
            //On récupère notre question
            Response resp = responses.get(indexResponse);
            //On retourne si c'est la bonne réponse
            return resp.isTruth();
        }

        //On retourne faux
        return false;
    }

    public void display() {

        //On affiche les réponses
        for (int i = 0; i < responses.size(); i++) {
            //On ajoute 1 à l'index pour éviter d'avoir le 0
            System.out.println((i+1) + ". " + responses.get(i).getText());
        }

    }

}
