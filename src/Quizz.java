import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Quizz {

    //Nom de notre quizz
    private final String name;
    //Liste des questions qui devront être posé
    private final List<Question> questions = new ArrayList<>();
    private final Scanner scanner;

    //Question sur laquelle on est
    private int questionIndex = 0;

    public Quizz(String name) {
        this.name = name;
        //Je place mon instance ici délibéremment, pour montrer plusieurs façon de faire.
        this.scanner = new Scanner(System.in);
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }

    public void start() {

        System.out.println("Bonjour sur notre quizz : " + name);
        System.out.println("Vous devez arrivez à répondre correctement à toute les réponses.");

        if(questions.size() > 0) {
            //méthode 1
            while(askQuestion()) {
                //Si on a la bonne réponse, on incrémente l'index
                this.questionIndex++;
                //On pourrait par exemple afficher un message aléatoire ici
            }
            //Methode 2
            //askQuestion();

            System.out.println("Voulez-vous recommencer ? (true or false)");
            if(scanner.nextBoolean()){
                //On reprend depuis le début à titre d'exemple, on pourrait faire autrement
                this.questionIndex = 0;
                start();
                return;
            }
            System.out.println("Aurevoir!");
        } else {
            //On pourrait afficher une erreur par exemple
            System.out.println("Aucune question n'est répertorié.");
        }
    }

    //Methode 1
    private boolean askQuestion() {

        if(questionIndex < this.questions.size()) {
            Question question = this.questions.get(questionIndex);
            question.display();
            if (question.valid(scanner.nextInt())) {
                System.out.println("Vous avez bien répondu ! Question suivante.");
                return true;
            } else {
                System.out.println("Quel erreur ! Ce n'est pas la bonne réponse.");
            }
        } else {
            System.out.println("Vous avez gagné !");
        }
        return false;
    }

    //methode 2
    /*
    private void askQuestion() {

        if(questionIndex < this.questions.size()) {
            Question question = this.questions.get(questionIndex);
            question.display();
            if (question.valid(scanner.nextInt())) {
                System.out.println("Vous avez bien répondu ! Question suivante.");
                this.questionIndex++;
                askQuestion(); //On rappelle la méthode
            } else {
                System.out.println("Quel erreur ! Ce n'est pas la bonne réponse.");
            }
        } else {
            System.out.println("Vous avez gagné !");
        }
    }*/
}
