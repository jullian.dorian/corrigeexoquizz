public class Response {

    private final String text;
    private final boolean truth;

    public Response(String text) {
        this(false, text);
    }

    public Response(boolean truth, String text) {
        this.text = text;
        this.truth = truth;
    }

    public String getText() {
        return text;
    }

    public boolean isTruth() {
        return truth;
    }
}
